const gulp = require('gulp');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify-es').default;
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const del = require('del');
const htmlMin = require('gulp-htmlmin');
const cssClean = require('gulp-clean-css');
const image = require('gulp-image');
const server = require('browser-sync').create('dev');
const prodServer = require('browser-sync').create('prod');

let isProd = false;

function sassTask() {
  return gulp
    .src('src/styles/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('src'));
}

function sassOnServe() {
  return sassTask().pipe(server.stream());
}

function babelTask() {
  return gulp
    .src('src/**/*.js')
    .pipe(
      babel({
        presets: ['@babel/env'],
        plugins: ['@babel/transform-runtime'],
      })
    )
    .pipe(gulp.dest('dist/babel-tmp'));
}

function compileAndUglify() {
  const b = browserify({
    entries: './dist/babel-tmp/index.js',
    debug: true,
  });

  return b
    .bundle()
    .pipe(source('index.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
}

function removeBabelTmpTask() {
  return del(['dist/babel-tmp']);
}

exports.serve = function serve(cb) {
  isProd = false;

  server.init({
    server: 'src',
    cors: true,
    notify: false,
    ui: false,
  });

  gulp.watch('src/**/*.scss', { ignoreInitial: false }, sassOnServe);
  gulp.watch(['src/**/*', '!src/**/*.+(css|scss)']).on('change', server.reload);

  return cb();
};

function html() {
  return gulp
    .src('src/index.html')
    .pipe(htmlMin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'));
}

function css() {
  return gulp
    .src('src/style.css')
    .pipe(cssClean({ compatibility: 'ie8' }))
    .pipe(gulp.dest('dist'));
}

function assets() {
  return gulp
    .src('src/assets/images/*.+(svg|gif)')
    .pipe(image())
    .pipe(gulp.dest('dist/assets/images'));
}

function production(cb) {
  isProd = true;

  prodServer.init({
    server: 'dist',
    cors: true,
    notify: false,
    port: 5500,
    ui: false,
  });
  return cb;
}

exports.clean = function() {
  return del(['dist/**']);
};

const javaScript = gulp.series(babelTask, compileAndUglify, removeBabelTmpTask);
exports.prod = gulp.series(javaScript, html, sassTask, css, assets, production);
