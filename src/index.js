import { getDeclension } from './helpers/getDeclension.js';
import { clearNode } from './helpers/clearContainer.js';
import { delay } from './helpers/delay.js';
import { renderTag } from './helpers/renderTag.js';
import './components/movieCard.js';
import './components/currentYear.js';
import { renderLoad } from './helpers/renderLoad.js';
import { appendMovie } from './helpers/appendMovie.js';
import { dataStorage } from './helpers/dataStorage.js';
import { search } from './helpers/search.js';

const resultsContainer = document.querySelector('.results__list');
const form = document.querySelector('.search__form');
const input = document.querySelector('.search__input');
const resulstHeader = document.querySelector('.results__header');
const tagsContainer = document.querySelector('.group_of_tags');
const buttonMore = document.querySelector('.downloadButton');
const buttonEsc = document.querySelector('.search__esc');
const searchContainer = document.querySelector('.search');
let tagsArray = [];
let page = 1;
const functionsArray = [
  renderLoad,
  getDeclension,
  renderTag,
  appendMovie,
  dataStorage,
];

const onSearch = (event) => {
  event.preventDefault();
  page = 1;
  clearNode(resultsContainer);
  clearNode(resulstHeader);
  search([input.value, tagsArray, page], functionsArray);
};

let isOneClick = true;

const clickTag = async (event) => {
  event.preventDefault();
  let target = event.target;
  if (target.classList.contains('tag')) {
    await delay(200);
    if (isOneClick) {
      input.value = target.textContent;
      clearNode(resultsContainer);
      clearNode(resulstHeader);
      page = 1;
      search([input.value, tagsArray, page], functionsArray);
    }
  }
};

const removeTag = async (event) => {
  let target = event.target;
  if (target.classList.contains('tag')) {
    target.remove();
    tagsArray = tagsArray.filter((item) => item !== target.textContent);
    localStorage.removeItem('tags');
    localStorage.setItem('tags', JSON.stringify(tagsArray));
    isOneClick = false;
  }
  await delay(200);
  isOneClick = true;
};

const clickMore = (event) => {
  event.preventDefault();
  page++;
  search([input.value, tagsArray, page], functionsArray);
};

const clickEsc = (event) => {
  let target = event.target;
  if (target.classList.contains('search__esc')) {
    event.preventDefault();
    input.value = null;
  }
};

const toSubmit = () => {
  searchContainer.addEventListener('click', clickEsc, true);
  form.addEventListener('submit', onSearch);
  tagsContainer.addEventListener('click', clickTag);
  tagsContainer.addEventListener('dblclick', removeTag);
  buttonMore.addEventListener('click', clickMore);
};

//главная функция
toSubmit();

// вызов из local storage поля ввода для рендеринга
input.value = localStorage.getItem('input');

//вызов из Local storage карточек для рендеринга
let storeData = dataStorage('Movie');

if (!!storeData) {
  let storeCount = storeData.totalResults;

  resulstHeader.textContent = `Нашли ${storeCount} ${getDeclension(
    storeCount
  )}`;
  resultsContainer.append(appendMovie(storeData));

  //вызов из local storage тегов для рендеринга
  let storeTags = dataStorage('tags');

  storeTags.forEach((tag) => {
    tagsContainer.prepend(renderTag(tag));
    tagsArray.push(tag);
  });

  if (page < Math.ceil(storeCount / 10)) {
    buttonMore.classList.remove('display_none');
  }

  //вызов из localstorage количества уже загруженных страниц
  page = storeData.pages;
}
