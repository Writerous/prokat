const params = ['poster', 'link'];
const mirror = (params, element) => {
  params.forEach((param) => {
    Object.defineProperty(element, param, {
      get() {
        return element.getAttribute(param);
      },
      set(value) {
        element.setAttribute(param, value);
      },
    });
  });
};
let newLink = null;

let newPoster = null;

class MovieCard extends HTMLElement {
  constructor() {
    super();
    mirror(params, this);
  }

  static get observedAttributes() {
    return ['poster', 'link'];
  }

  attributeChangedCallback(param, oldValue, newValue) {
    this.innerHTML = `<div class="card">
          <a href="" class="card-link">
            <img class="card-image" src="" alt="poster" /> 
          </a>
         </div>`;
    switch (param) {
      case 'poster':
        newPoster = newValue;
        if (newValue !== 'N/A') {
          this.querySelector('.card-image').src = newValue;
        }
        this.querySelector('.card-link').href = newLink;
        break;
      case 'link':
        newLink = newValue;
        this.querySelector('.card-link').href = newValue;
        if (newPoster !== 'N/A') {
          this.querySelector('.card-image').src = newPoster;
        }
        break;
    }
  }
}

customElements.define('movie-card', MovieCard);
