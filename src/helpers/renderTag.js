export const renderTag = (searchTerm) => {
  let tag = document.getElementById(searchTerm);
  if (tag) {tag.remove()}
  else
    { tag = document.createElement('a');
      tag.classList.add('tag');
      tag.href = `/?search=${searchTerm}`;
      tag.id = searchTerm;
      tag.innerHTML = searchTerm;
    }
    return tag;
}