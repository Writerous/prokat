export function delay(ms) {
  // eslint-disable-next-line no-shadow
  return new Promise(resolve => setTimeout(resolve, ms));
}