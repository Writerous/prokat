export const search = async (args, functionsArray) => {
  const [renderLoad, getDeclension, renderTag, appendMovie, dataStorage] = functionsArray;

  const [searchString, tagsArray, page] = args;

  const resultsContainer = document.querySelector('.results__list');
  const resultsLoader = document.querySelector('.results_loader');
  const resulstHeader = document.querySelector('.results__header');
  const tagsContainer = document.querySelector('.group_of_tags');
  const buttonMore = document.querySelector('.downloadButton');
 
  const searchTerm = searchString.toLocaleLowerCase();

  try {
     let data = (Object.keys(localStorage).some(key => key === `${searchTerm}`) && (page === 1)) ? dataStorage(`${searchTerm}`) : await fetch(
      `http://www.omdbapi.com/?apikey=9e74febe&type=movie&s=${searchTerm}&page=${page}`
    ).finally(renderLoad(resultsLoader)).then((r) => r.json());
    //сохранение  local storage
    //создание объекта для сохранения 
    let JSONconcat = {};

    JSONconcat.totalResults = data.totalResults;
    JSONconcat.Responce = data.Responce;
    JSONconcat.pages = page;
    JSONconcat.Search = page === 1 ? data.Search : [...dataStorage('Movie').Search, ...data.Search];
    localStorage.setItem('Movie', JSON.stringify(JSONconcat));
    localStorage.setItem(`${searchTerm}`, JSON.stringify(JSONconcat));  
    localStorage.removeItem('input');
    localStorage.setItem('input', searchString);

    //рендеринг
    resultsLoader.classList.add('display_none');
    const count = data.totalResults;

    if (count) {
      resulstHeader.textContent = `Нашли ${count} ${getDeclension(count)}`;
      tagsContainer.prepend(renderTag(searchTerm));

      //добавляем теги в local storage
      if (tagsArray.length > 1 ) {
         tagsArray.forEach((element, index) => {
         if (element === searchTerm) {
            tagsArray.splice(index, 1); 
         } 
        });
      }
      tagsArray.push(searchTerm);
      localStorage.setItem('tags', JSON.stringify(tagsArray)); 

      // продолжаем рендерить
      resultsContainer.append(appendMovie(data));

      //отрисовка кнопки доп загрузки
       if (page < Math.ceil(count / 10)) {
        buttonMore.classList.remove('display_none');
      } 
    } else {
      resulstHeader.textContent = `Мы не поняли о чем речь ¯\\_(ツ)_/¯ `;
      buttonMore.classList.add('display_none');     
    }
  } catch {
    resulstHeader.textContent = `аааа все поломалось!(`;
    resultsLoader.classList.add('display_none');
    
  }
}