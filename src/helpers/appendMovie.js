const renderMovies = (movieData) => {
  // Создаем элемент и заполняем его атрибуты через свойства объекта
  const movie = document.createElement('movie-card');
  movie.poster = movieData.Poster;
  movie.link = `https://www.imdb.com/title/${movieData.imdbID}/`;
  return movie;
};

export const appendMovie = (data) => {
  const movies = data.Search.map((result) => renderMovies(result));

    const fragment = document.createDocumentFragment();

    movies.forEach((movie) => fragment.appendChild(movie));
    return fragment;
}